const express = require('express');
const { resolve } = require('path');
const router = require('./routing');
const app = express();

app.set('view engine', 'pug');

app.use(express.static(resolve('static')));
app.use(express.json());
app.use(router);

module.exports = app;