const express = require('express');
const router = express.Router();
const {
  readTodos,
  readTodo,
  createTodo,
  updateTodo,
  deleteTodo,
} = require('../controllers/api-controllers');

const {
  indexPage,
  homePage,
  servicesPage,
  todosPage,
  redirectURL
} = require('../controllers/app-controllers');

// API ROUTES
router.get('/api/todos', readTodos);
router.get('/api/todos/:id', readTodo);
router.post('/api/todos', createTodo);
router.put('/api/todos/:id', updateTodo);
router.delete('/api/todos/:id', deleteTodo);


// APP ROUTES
router.get('/', indexPage);
router.get('/home', homePage);
router.get('/services', servicesPage);
router.get('/services/:username', servicesPage);
router.get('/todos', todosPage);
router.get('*', redirectURL);

module.exports = router;