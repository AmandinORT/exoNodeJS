const { resolve } = require('path');
exports.indexPage = (req, res) => {
  let firstname = 'Amandin';
  res.render('index', { firstname });
};

exports.homePage = (req, res) => {
  res.render('home');
};

exports.servicesPage = (req, res) => {
  res.render('services', {
    username: req.params.username
  });
  // res.sendFile(resolve('services.html'));
};

exports.todosPage = (req, res) => {
  res.render('todos', {
    todos : require('../database/data.json').todos
  });
}

exports.redirectURL = (req, res) => {
  res.redirect('/');
};
