const { resolve } = require('path');
const todosJSONFile = resolve('database', 'data.json');
const { todos } = require('../database/data.json');
const { randomUUID } = require('crypto');
const { writeFile } = require('fs').promises;

exports.readTodos = (req, res) => {
  res.json(todos);
};

exports.readTodo = (req, res) => {
  if(req.params.id) {
    const todo = todos.find(todo => {
      return todo.id == req.params.id;
    });
    res.json(todo);
  }
};

exports.createTodo = (req, res) => {
  const { text } = req.body;

  if (text) {
    todos.push({
      id: randomUUID(),
      text,
      done: false
    });
    updateDBandSendResponse(res);
  } else {
    res.status(500).json({ status: 'Corps de requête vide.'});
  }
}

exports.updateTodo = (req, res) => {
  if(req.params.id) {
    if (req.body) {
      const todo = todos.find(todo => {
        return todo.id == req.params.id;
      });
      const index = todos.indexOf(todo.id);
      todo.text = req.body.text;
      todo.done = req.body.done;
      todos[index] = todo;
      updateDBandSendResponse(res);
    } else {
      res.status(500).json({ status: 'Corps de requête vide.'});
    }
  } else {
    res.status(500).json({ status: 'Cette todo n\'existe pas.'});
  }
}

exports.deleteTodo = (req, res) => {
  if(req.params.id) {
    const index = todos.indexOf(
      todos.find(todo => {
      return todo.id == req.params.id;
      })
    );
    todos.splice(index, 1);
    updateDBandSendResponse(res);
  } else {
    res.status(500).json({ status: 'Cette todo n\'existe pas.'});
  }
}

function updateDBandSendResponse(res) {
  // { todos } pour ne pas supprimer la clé, .promises added to path to return a promise below
  writeFile(todosJSONFile, JSON.stringify({ todos }, null, 2))
    .then(() => res.status(200).json({ status: 'success' }))
    .catch(() => res.status(500).json({ status: 'error' }));
}